//Article class
//Constructor function
//Instantiate in a var to create
function Article(heading, text, author, imageURL){
	this.heading = heading;
	this.text = text;
	this.author = author;
	this.imageURL = imageURL;
	this.marked = false;
	//this.commentsList = [{name:"lakss", message:"Ingvars saxparty 8 är bra"}]; //List consisting of comment objects. Use this when the comment class exists.
};