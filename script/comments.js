var commentInputApp = angular.module("commentInputApp", []);


commentInputApp.controller("inputCtrl",["$scope", "manager", "activeSlide", "$firebaseArray", function($scope, manager, activeSlide, $firebaseArray){

	
	$scope.isAdmin=true;
	$scope.isVisual= false;

	$scope.articleList = manager.db();
	$scope.activeSlideIndex;
	$scope.articleImage;
	$scope.articleHeading;
	$scope.articleComments;

	$scope.$watch(function () { return activeSlide.index; }, function () {
      $scope.activeSlideIndex = activeSlide.index;
      if ($scope.activeSlideIndex >= 0) {
      	$scope.specificArticle = $scope.articleList[$scope.activeSlideIndex];
      	$scope.ref = new Firebase("https://newsienews.firebaseio.com/"+$scope.specificArticle.$id);
      	$scope.articleComments = $firebaseArray($scope.ref.child("commentsList"));
      }
    });

    $scope.addComment = function(name, message) {	
    	if($scope.activeSlideIndex >= 0){
    		console.log("in addComment() " + $scope.activeSlideIndex);
    		if ((name != null) && (message != null)) {
    			$scope.articleComments.$add({name:name, message:message});
    		}		
    	}
    	alert(name+", din kommentar är adderad.");	
    	var nameField = document.getElementById("nameField");
		nameField.value = "";
		var commentField = document.getElementById("commentField");
		commentField.value = "";
    };

	$scope.comments=[
					{name: "Rohini", comment: "Jag tycker att sidan är fin.. alla är så duktiga!"},
					{name: "David", comment: "Såja det är väl inte så farlig.. kramas ?"},	
					{name: "Dan", comment: "Kom inte här och snacka, ska du ha stryk ?"},
					{name: "Anders", comment:"Den här sidan verkar ju vara skräp!"}
					];

	$scope.comments.name = "";
	$scope.comments.comment = "";
	

	// $scope.addComment = function(){
	// 	console.log("antal kommentarer: "+$scope.comments);
	// 	var comment = {
	// 			name: $scope.comments.nameInput, 
	// 			comment:$scope.comments.commentInput
	// 	};
	// 	$scope.comments.splice(0, 0, comment);
	// 	$scope.isVisual = true;
	// 	alert(comment.name +", din kommentar är nu adderad till listan!");
	// 	$scope.comments.nameInput= null;
	// 	$scope.comments.commentInput = null ;
	// }
/*
	$scope.removeComment = function(index){
		alert("Tar bort kommentaren");
		$scope.comments.splice(index,1);
	}*/

	$scope.showComments =function(){
		if($scope.isVisual=== false){
		$scope.isVisual= true;
		console.log("if");
		}else {
			$scope.isVisual= false;
			console.log("else");
		}
	}


}]);

