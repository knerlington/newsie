var mod = angular.module("managerModule", ["firebase", "naif.base64"]);
mod.factory("manager", ["$firebaseArray",function($firebaseArray){
	
	return {
		db: function(){
			console.log("db");
			var db = new Firebase("https://newsienews.firebaseio.com/");
			var list = [];
			list = $firebaseArray(db);
			console.log("Items in firebase db:"+list.length);
			return list;
		},
		childList: function(){
			var child;
			child = this.fbDB().child("commentsList");
			return child;
		}, 
		childDB: function(){
			var fb = new Firebase("https://newsienews.firebaseio.com/");
			var list = [];
			list = $firebaseArray(fb.child("commentsList"));
			return list;
		}
		
	};
	
}]);

mod.controller("articleController", ["$scope", "manager", "$firebaseArray", function($scope, manager, $firebaseArray){
	$scope.heading;
	$scope.text;
	$scope.author;

	$scope.makeCommentsVisible = false;
	$scope.comments = [];
	$scope.ref;

	$scope.articleList = manager.db();
	

	$scope.addArticle = function(){
		if(!$scope.heading){
			$scope.heading = "Heading placeholder";
		}
		
		// Enables an article to be saved without a picture
		if (!$scope.photo) {
			$scope.image = "";
			console.log("No image");
		} else {
			$scope.image = $scope.photo.base64;
			console.log("Image selected");
		}

		var article = new Article($scope.heading, $scope.text, $scope.author, $scope.image);
		//$scope.articleList.push(article);
		console.log($scope.articleList.length);
		for(var i = 0; i < $scope.articleList.length; i++){
			console.log($scope.articleList[i].heading);	
		}
		$scope.articleList.$add(article);
		alert("Adderat artikel med titel: "+$scope.heading);
		$scope.heading =null;
		$scope.text = null;
		$scope.author = null ;

		var imageFileName = document.getElementById("imageFileName");
		imageFileName.value = "";

		var imagePreview = document.getElementById("imagePreview");
		imagePreview.style.visibility = "hidden";

	};
		$scope.removeArticle = function(index){
		console.log("tar bort artikel nr: "+index);
		$scope.articleList.$remove(index);
		alert("Article removed!");
	};	

		$scope.showImage = function(){
		imagePreview.style.visibility = "visible";	
		console.log("visible");
		}
		
		$scope.getImage = function(imgData) {
			if (!imgData) {
				return '../images/default.png';
			} else {
				return 'data:image/gif;base64, ' + imgData;
			}
		}

		/*$scope.showComments = function(objectId) {
			ref = new Firebase("https://newsienews.firebaseio.com/"+objectId);
			$scope.comments = $firebaseArray(ref.child("commentsList"));
			console.log("object id: " + objectId);
			console.log("entire path : " + ref);
			$scope.makeCommentsVisible = true;

			$scope.hideComments = function() {
			$scope.makeCommentsVisible = false;
		}
		}*/

		$scope.getCommentsList = function(articleId) {
			console.log("artid:"+articleId);
			if(!$scope.abc) {
				$scope.abc = {};
			}
			if(!$scope.abc[articleId]) {
				console.log("hej");
				var ref = new Firebase("https://newsienews.firebaseio.com/"+articleId+"/commentsList/");
				$scope.abc[articleId] = $firebaseArray(ref);	
			}
			
			return $scope.abc[articleId];
		};

		$scope.removeComment = function(articleId,id) {
			console.log("article id: " + articleId);
			console.log("comment id: " + id);
			$scope.ref = new Firebase("https://newsienews.firebaseio.com/"+articleId);
			$scope.ref.child("commentsList").child(id).remove();
		}
		
}]);

