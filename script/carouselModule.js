  var app = angular.module('carouselModule',['ui.bootstrap']);
  
  app.filter('reverseArray', function() {
    return function(items) {
      return items.slice().reverse();
    }
  });

  app.factory('activeSlide', function($rootScope) {
    'use strict';
    var service = {};
    service.index = -1;

    service.broadcast = function (index) {
      $rootScope.$broadcast('activeSlide.update', index);
    };

    service.update = function (newIndex) {
      this.index = newIndex;
      this.broadcast(newIndex);
    };
    return service;
  });

  app.controller('CarouselDemoCtrl', ['$scope', 'manager', 'activeSlide', function ($scope,manager,activeSlide) {
  $scope.articleDetails;
  $scope.articleHeading;  
  $scope.articleAuthor;
  $scope.articles = manager.db();

  $scope.getFirstLine = function(articleText) {
    var firstLine =  articleText.substring(0, articleText.indexOf("."));
   // var firstLine =  articleText.substring(0, 2);
    return firstLine + "...";
  };
  
 $scope.$watch(function () {
    for (var i=0; i<$scope.articles.length; i++) {
      if ($scope.articles[i].active) {
        activeSlide.update(i);
        $scope.articleDetails = $scope.articles[i].text;
        $scope.articleHeading = $scope.articles[i].heading;
        $scope.articleAuthor = $scope.articles[i].author;
        if (!$scope.articles[i].imageURL) {
           $scope.articleImage = '../images/default.png';  
        } else {
          $scope.articleImage = "data:image/gif;base64, "+ $scope.articles[i].imageURL; 
        }
      }
    }
  });

}]);
