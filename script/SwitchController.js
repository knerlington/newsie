angular.module("switch", [])
.controller("SwitchController", ["$scope", function($scope) {

  $scope.items = ["Create", "Remove", "Default"];
  $scope.hello = "Push da button";
  $scope.selection = $scope.items[3];



  $scope.switchTo = function(state) {

		if(state == 'Create') {
			$scope.hello = "Push another button";
			$scope.selection = $scope.items[0];

		} else if (state == "Remove") {
			$scope.hello = "DON'T DO IT!!";
			$scope.selection = $scope.items[1];
		}


	};


}]);
